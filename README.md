Theia Desktop IDE standalone app
================================

Wraps the theia IDE for the browser with a Qt WebEngine Desktop app.

Based on https://theia-ide.org/docs/composing_applications

Tested on Debian 11 (bullseye).

## Prerequisites

```sh
sudo apt install yarnpkg git qtwebengine5-dev node-gyp qt5-qmake qtbase5-dev qt5-default libsecret-1-dev
git clone https://gitlab.com/simevo/theia-app
```

## Building

```sh
cd theia-app
yarnpkg
qmake
make
```

## Installing

- adapt `theia-app.desktop`

- install the desktop entry (adapt to your actual path):
```
sudo ln -s "$HOME/src/theia-app/theia-app.desktop /usr/share/applications/theia-app.desktop
```

## Known issues

- when the webview opens, the web server is still starting, the user has to hit "reload"; it would be nice to have a splash screen and show the webview only once the web server is started

- the web server is run on localhost:3000; it would be nice to have it on a segregated VLAN

- the webview and the web server communicate over http protocol; it would be nice to use https

## License

BSD 3-Clause, see [LICENSE](/LICENSE) file.
