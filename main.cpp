/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** Copyright (C) 2020 Paolo Greppi <paolo.greppi@libpf.com>
**
** Based in part on the WebEngine Widgets Minimal Example from the 
** examples of the Qt Toolkit:
** https://doc.qt.io/qt-5/qtwebengine-webenginewidgets-minimal-example.html
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QApplication>
#include <QWebEngineView>
#include <QProcess>
#include <QIcon>

void killProcessGroup(qint64 parentProcessId) {
  if (parentProcessId == 0)
    return;
  qDebug() << "killProcessGroup for parent id:" << parentProcessId;
  QProcess get_gid;
  QStringList get_gid_cmd;
  get_gid_cmd << "-p" << QString::number(parentProcessId) << "-o" << "pgid" << "--no-heading";
  get_gid.start("ps", get_gid_cmd);
  get_gid.waitForFinished();
  auto gid(get_gid.readAllStandardOutput());
  QProcess::execute("kill", QStringList() << "-9" << gid.trimmed());
}

int main(int argc, char *argv[]) {
  QCoreApplication::setOrganizationName("simevo");
  QCoreApplication::setApplicationName("theia-app");
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication app(argc, argv);

  app.setWindowIcon(QIcon("./icon.svg"));

  QProcess process;
  process.setProgram("./node_modules/@theia/cli/bin/theia");
  process.setArguments({"start", "--plugins=local-dir:plugins"});
  process.setWorkingDirectory(".");
  process.start();

  QWebEngineView view;
  view.setUrl(QUrl(QStringLiteral("http://localhost:3000")));
  view.setZoomFactor(1.2);
  view.show();

  auto res = app.exec();
  killProcessGroup(process.processId());
  return res;
}
